package beck.helfenhelfen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "RecyclerViewExample";
    private List<FeedItem> feedsList;
    private RecyclerView mRecyclerView;
    private MyRecyclerViewAdapter adapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        feedsList = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        adapter = new MyRecyclerViewAdapter(MainActivity.this, feedsList);
        mRecyclerView.setAdapter(adapter);

        createList();

        //String url = "http://stacktips.com/?json=get_category_posts&slug=news&count=30";
        //new DownloadTask().execute(url);
    }

    public void createList() {
        FeedItem feedItem = new FeedItem();
        feedItem.setThumbnail(getDrawable(R.drawable.cat));
        feedItem.setTitle("cat");
        feedsList.add(feedItem);
    }

//    private void parseResult(String result) {
//        try {
//            JSONObject response = new JSONObject(result);
//            JSONArray posts = response.optJSONArray("posts");
//            feedsList = new ArrayList<>();
//
//            for (int i = 0; i < posts.length(); i++) {
//                JSONObject post = posts.optJSONObject(i);
//                FeedItem item = new FeedItem();
//                item.setTitle(post.optString("title"));
//                item.setThumbnail(post.optString("thumbnail"));
//                feedsList.add(item);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

//    public class DownloadTask extends AsyncTask<String, Void, Integer> {
//
//        @Override
//        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected Integer doInBackground(String... params) {
//            Integer result = 0;
//            HttpURLConnection urlConnection;
//            try {
//                URL url = new URL(params[0]);
//                urlConnection = (HttpURLConnection) url.openConnection();
//                int statusCode = urlConnection.getResponseCode();
//
//                // 200 represents HTTP OK
//                if (statusCode == 200) {
//                    BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//                    StringBuilder response = new StringBuilder();
//                    String line;
//                    while ((line = r.readLine()) != null) {
//                        response.append(line);
//                    }
//                    parseResult(response.toString());
//                    result = 1; // Successful
//                } else {
//                    result = 0; //"Failed to fetch data!";
//                }
//            } catch (Exception e) {
//                Log.d(TAG, e.getLocalizedMessage());
//            }
//            return result; //"Failed to fetch data!";
//        }
//
//        @Override
//        protected void onPostExecute(Integer result) {
//            progressBar.setVisibility(View.GONE);
//
//            if (result == 1) {
//                adapter = new MyRecyclerViewAdapter(MainActivity.this, feedsList);
//                mRecyclerView.setAdapter(adapter);
//            } else {
//                Toast.makeText(MainActivity.this, "Failed to fetch data!", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
}
