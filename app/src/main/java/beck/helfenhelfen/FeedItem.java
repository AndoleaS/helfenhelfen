package beck.helfenhelfen;

import android.graphics.drawable.Drawable;

public class FeedItem {
    private String title;
    private Drawable thumbnail;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Drawable thumbnail) {
        this.thumbnail = thumbnail;
    }
}
